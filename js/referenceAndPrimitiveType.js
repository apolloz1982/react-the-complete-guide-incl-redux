// Primitive
const number = 1;
const num2 = number;

console.log(num2);

//Reference
const person = {
  name: 'Max'
};
const secondPerson = person; // same reference
const secondPerson = {...person} // difference refecence, same as new object
person.name = 'Manu'
console.log(person);
console.log(secondPerson);