//Declare with var
var myName = 'Max';
console.log(myName);
myName = 'Menu';
console.log(myName);

// Declare with let
let myName = 'Max';
console.log(myName);
myName = 'Menu';
console.log(myName);

// Declare with const
const myName = 'Max';
console.log(myName);
myName = 'Menu';
console.log(myName);