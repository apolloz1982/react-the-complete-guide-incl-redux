const numbers = [1, 2, 3];
[num1, num2] = numbers;
console.log(num1, num2);

const person = {
  name: 'Max',
  age: 28
};
const {name, age} = person;
console.log(name, age); 
