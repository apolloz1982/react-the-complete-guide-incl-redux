// Declare function
function printMyName(name){
  console.log(name);
}
// Call function
printMyName('Max');
// Declare arrow function
const printMyName = name =>{
  console.log(name);
}
// Call arrow funtion
printMyName('Max')
// Declare arrow function without parameter
const printMyName = () =>{
  console.log('Max');
}
// Call arrow funtion without parameter
printMyName()
// Declare arrow function multiple parameter
const printMyName = (name, age) => {
  console.log(name, age);
}
// Call arrow funtion multiple parameter
printMyName('Max', 28);

// Declare arrow funtion return
const multiply = (num) => {
  return num * 2;
}
// Declare arrow funtion return (Single line)
const multiply = (num) => num * 2;
// Call arrow funtion return
console.log(multiply(2));


